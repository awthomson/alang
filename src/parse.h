#include <stdio.h>
// #include <stdlib.h>
// #include <string.h>
// #include <sys/ioctl.h>

int parse(char *fname);
void gobble_end_of_line(FILE *f, int *fpos);
void gobble_whitespace(FILE *f, int *fpos);
void get_word_token(FILE *f, int *fpos, char *token);
void get_quote_token(FILE *f, int *fpos, char *token);
void get_number(FILE *f, int *fpos, char *token);
