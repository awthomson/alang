#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>

#include "parse.h"

int parse(char *fname) {
	
	char txt[1000];
	char tmp_str[1000];
	FILE *fptr;
	fptr = fopen(fname,"r");
	if (fptr == NULL) {
		printf("Error! opening file");
		return 1;
	}

	int fpos = 0;

	int keep_going = 1;
	
	while (keep_going) {	
		// printf("POS: %d\n", fpos);
		fseek(fptr, fpos, SEEK_SET);
		char c = fgetc(fptr);
		printf("C: %c\n", c);

		char token[1024];

		/* Get the next token
			 	TODO: Comment blocks
				TODO: Speech marks
				TODO: Quote marks
		*/
	
		if (c == EOF) {
			keep_going = 0;
		} else if (c == '#') {
			// Comments
			gobble_end_of_line(fptr, &fpos);
		} else if ((c == ' ') || (c == '\n') || (c == '\t')) {
			// Ignore whitespace
			gobble_whitespace(fptr, &fpos);
		} else if ((c >='a') && (c <= 'z') || (c >= 'A') && (c <= 'Z')) {
			// Word tokens
			get_word_token(fptr, &fpos, token);
			printf("TOKEN: %s\n", token);
		} else if (c =='\"') {
			// Qutoes
			get_quote_token(fptr, &fpos, token);
			printf("TOKEN: %s\n", token);
		} else if ((c >='0') && (c <= '9')) {
			// Numbers (int only for now)
			get_number(fptr, &fpos, token);
			printf("TOKEN: %s\n", token);
		} else if (	(c=='{') || (c=='}') ||
						(c=='(') || (c==')') ||
						(c=='[') || (c==']') ||
						(c=='-') || (c=='+') ||
						(c=='*') || (c=='/') ||
						(c=='%') || (c=='.') ||
						(c=='|') || (c=='&') ||
						(c==';') || (c=='!') ) {
			// Single character tokens
			token[0]=c;
			token[1]=0;
			printf("TOKEN: %s\n", token);
			fpos++;
		} else {
			printf("Unhandled character: %c\n", c);
			fpos++;
		}
	}
}

// TODO: Limit # characters read
void get_quote_token(FILE *f, int *fpos, char *token) {
	int keep_going = 1;
	int pos = 0;
	fpos[0]++;
	while (keep_going) {
		fseek(f, *fpos, SEEK_SET);
		char c = fgetc(f);
		if (c!='"') {
			fpos[0]+=2;
			token[pos] = c;
			pos++;
		} else {
			token[pos]=0;
			keep_going = 0;
		}
	}
	return;
}

// TODO: Limit # characters read
void get_number(FILE *f, int *fpos, char *token) {
	int keep_going = 1;
	int pos = 0;
	while (keep_going) {
		fseek(f, *fpos, SEEK_SET);
		char c = fgetc(f);
		if ((c>='0')&&(c<='9')) {
			fpos[0]++;
			token[pos] = c;
			pos++;
		} else {
			token[pos]=0;
			keep_going = 0;
		}
	}
	return;
}

// TODO: Limit # characters read
void get_word_token(FILE *f, int *fpos, char *token) {
	int keep_going = 1;
	int pos = 0;
	while (keep_going) {
		fseek(f, *fpos, SEEK_SET);
		char c = fgetc(f);
		if ((c>='a')&&(c<='z') || (c>='A')&&(c<='Z')) {
			fpos[0]++;
			token[pos] = c;
			pos++;
		} else {
			token[pos]=0;
			keep_going = 0;
		}
	}
	return;
}


void gobble_whitespace(FILE *f, int *fpos) {
	int keep_going = 1;
	while (keep_going) {
		fseek(f, *fpos, SEEK_SET);
		char c = fgetc(f);
		if ((c != '\n') && (c != '\t') && (c != ' ')) {
			keep_going = 0;
		} else {
			fpos[0]++;
		}
	}
	return;
}

void gobble_end_of_line(FILE *f, int *fpos) {
	int keep_going = 1;
	while (keep_going) {
		fseek(f, *fpos, SEEK_SET);
		char c = fgetc(f);
		if ((c == '\n') || (c == EOF)) {
			keep_going = 0;
		} else {
			fpos[0]++;
		}
	}
	return;
}
