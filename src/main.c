#include <stdio.h> 
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

#include "parse.h"

void display_help(void);

int main (int argc, char **argv) {

	// Parse command-line arguments
	int c;
    char *config_file = 0;
    char *filename = 0;
    static struct option long_options[] = {
        {"help",		no_argument, 		0, 'h'},
        {"config-file",	required_argument, 	0, 'c'},
        {NULL, 0, NULL, 0}
    };
    int option_index = 0;
    while ((c = getopt_long(argc, argv, "c:fh", long_options, &option_index)) != -1) {
        switch (c) {
        case 'c':
            printf ("option c with value '%s'\n", optarg);
            config_file = optarg;
            break;
        case 'h':
            display_help();
            exit (0);
        case '?':
            break;
        default:
            printf ("?? getopt returned character code 0%o ??\n", c);
        }
    }

	// Couldn't find a filename in argument list
	if (argv[optind] == NULL) {
		printf("A filename is required.\n\n");
		display_help();
		exit(2);
	} else {
		filename = argv[optind];
	}

	// Additional unexpected arguments supplied
    if (optind+1 < argc) {
		printf("Additional parameters supplied.\n\n");
		display_help();
		exit(1);
    }

	parse(filename);
   exit (0);
}

void display_help(void) {
	printf("Usage:\n");
	printf("  md [options] FILENAME\n\n");
	printf("Options:\n");
	printf("  -c --config-file     Config file to  use\n");
	printf("  -h --help            Display usage\n");
	printf("\n");
}

